import json
import boto3

from transactions.services import process_transactions_file_content
from infrastructure.mailing import generate_email, send_email


def lambda_handler(event, context):
    # Get the S3 bucket and key from the event
    s3_bucket = event['Records'][0]['s3']['bucket']['name']
    s3_key = event['Records'][0]['s3']['object']['key']

    # Read the content of the S3 file
    s3 = boto3.client('s3')
    response = s3.get_object(Bucket=s3_bucket, Key=s3_key)
    file_content = response['Body'].read().decode('utf-8')

    transactions_summary = process_transactions_file_content(file_content)
    email_body = generate_email(transactions_summary)

    send = send_email(email_body)

    # Save email to file if not sent
    if not send:
        s3.put_object(
            Bucket=s3_bucket, 
            Key="email.html", 
            Body=email_body
            )


    # Process the file content (add your own processing logic here)
    processed_content = "Processed content: " + file_content

    # Return the processed content as the response
    return {
        'statusCode': 200,
        'body': json.dumps(processed_content)
    }