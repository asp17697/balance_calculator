from datetime import datetime
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def generate_email(summary_data):
    total_balance = summary_data['balance']
    avg_debit_amount = summary_data['debit_average']
    avg_credit_amount = summary_data['credit_average']
    
    html_template = f"""
        <!DOCTYPE html>
        <html>
        <head>
            <title>Financial Summary</title>
        </head>
        <body>
            <h1>Financial Summary</h1>
            <p>Total Balance: {total_balance}</p>
        """
    
    # Loop over the summaries and add them to the HTML template
    for month_key, mont_value in summary_data["transactions_by_month"].items():
        month_name = datetime.strptime(month_key, "%Y/%m").strftime("%B %Y")
        
        html_template += f"""
            <p>Average Transactions ({month_name}): {mont_value}</p>
            """
        
    html_template += f"""
        <ul>
            <li>Average debit amount: {avg_debit_amount}</li>
            <li>Average credit amount: {avg_credit_amount}</li>
        </ul>
        <hr>
        """
    
    # Add the logo to the HTML template
    html_template += """
        <img src="logo.png" alt="Logo">
    </body>
    </html>
    """
    return html_template

def send_email(email_body: str):
    # Send Email
    message = MIMEMultipart("alternative")
    message["Subject"] = get_subject()
    message["From"] = get_sender_email()
    message["To"] = get_recipient_email()
    message.attach(MIMEText(email_body, "html"))

def get_subject():
    # Here should be the logic to get the subject
    return "Summary of Account Transactions"

def get_sender_email():
    # Here should be the logic to get the sender email
    return "info@yourfavoritecreditcard.com"

def get_recipient_email():
    # Here should be the logic to get the recipient email
    return "debtor@gmail.com"