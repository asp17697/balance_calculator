import os
import boto3
from datetime import datetime
from io import StringIO
from botocore.exceptions import ClientError

from infrastructure.settings import WORKING_WITH_S3


class Logger:
    def __init__(self):
        if WORKING_WITH_S3:
            self.s3 = boto3.client('s3')
            self.file_exists = self._check_file_exists()
            if self.file_exists:
                file = self.s3.get_object(Bucket=os.environ.get("AWS_BUCKET"), Key="logs.txt")
                file = file["Body"].read().decode("utf-8")
                self.file = StringIO(file)

            else:
                self.file = StringIO()
        
        else:
            self.file = open("logger/logs.txt", "a")

    def _check_file_exists(self):
        try:
            self.s3.Object(os.environ.get("AWS_BUCKET"), "logs.txt").load()
            return True
        except ClientError as e:
            return False
        
    def _upload_file(self):
        self.s3.put_object(
            Bucket=os.environ.get("AWS_BUCKET"), 
            Key="logs.txt", 
            Body=self.file.getvalue()
            )
        
    def info(self, message: str):
        if WORKING_WITH_S3:
            file += f"{datetime.now()} - warn - {message}\n"
            self._upload_file()
        else:
            self.file.write(f"{datetime.now()} - info - {message}\n")

    def debug(self, message: str):
        if WORKING_WITH_S3:
            file += f"{datetime.now()} - error - {message}\n"
            self._upload_file()
        else:
            self.file.write(f"{datetime.now()} - debug - {message}\n")