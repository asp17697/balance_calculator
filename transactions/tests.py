from unittest import TestCase
from decimal import Decimal
from transactions.services import process_transactions_file_content

class TestProcessTransactionsFileContent(TestCase):
    def test_process_transactions_file_content(self):
        file_content = """
        id,date,transaction
        0,2023/7/15,60.5
        1,2023/7/28,-10.3
        2,2023/8/2,-20.46
        3,2023/8/13,10
        """
        transactions_summary = process_transactions_file_content(file_content)
        self.assertEqual(transactions_summary["transactions_count"], 4)
        self.assertEqual(transactions_summary["transactions_by_month"], {
            "2023/07": 2,
            "2023/08": 2
        })
        self.assertEqual(transactions_summary["credit_average"], Decimal("35.4"))
        self.assertEqual(transactions_summary["debit_average"], Decimal("-15.38"))
        self.assertEqual(transactions_summary["balance"], Decimal("20.12"))

