import csv
from datetime import datetime
from decimal import Decimal
from io import StringIO

from transactions.models import Transaccion
from transactions.crud import TransactionsDB
from logger.services import Logger

def process_transactions_file_content(file_content: str) -> dict:
    transactions_data = {
        "transactions_by_month": {},
        "transactions_count": 0
    }
    credit_amounts = []
    debit_amounts = []
    
    with TransactionsDB() as db:
        reader = csv.reader(StringIO(file_content))
        next(reader) # Skip header

        for row in reader:
            try:
                transaction = Transaccion(
                    id=int(row[0]),
                    date=row[1],
                    amount=Decimal(row[2])
                )
            except ValueError as e:
                Logger().debug(f"Invalid transaction: {row} - {e}")
                continue

            db.write(transaction) # Save transaction to database

            transactions_data["transactions_count"] += 1

            # Group transactions amount by credit and debit
            if transaction.amount < Decimal(0):
                credit_amounts.append(transaction.amount)
            else:
                debit_amounts.append(transaction.amount)

            # Group transactions by month
            month_key = datetime.strptime(transaction.date, "%Y/%m/%d").strftime("%Y/%m")
            
            if month_key not in transactions_data["transactions_by_month"]:
                transactions_data["transactions_by_month"][month_key] = 0

            transactions_data["transactions_by_month"][month_key] += 1
    
    transactions_data["credit_average"] = sum(credit_amounts) / len(credit_amounts) if len(credit_amounts) else Decimal(0)
    transactions_data["debit_average"] = sum(debit_amounts) / len(debit_amounts) if len(debit_amounts) else Decimal(0)
    transactions_data["balance"] = sum(debit_amounts) + sum(credit_amounts)

    return transactions_data

    

