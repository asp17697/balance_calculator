import os
from warnings import warn
import psycopg2

from transactions.models import Transaccion
from logger.services import Logger

class TransactionsDB:
    def __init__(self):
        self.conn_available = self._conn_available()
        if self.conn_available:
            self.conn = psycopg2.connect(
                host = os.environ.get("DB_HOST", "localhost"),
                port = os.environ.get("DB_PORT", 5432),
                database = os.environ.get("DB_NAME", "transactions"),
                user = os.environ.get("DB_USER", "postgres"),
                password = os.environ.get("DB_PASSWORD", "postgres")
            )

    def _conn_available(self):
        # Check if the postgresql database is available and table exists
        try:
            conn = psycopg2.connect(
                host = os.environ.get("DB_HOST", "localhost"),
                port = os.environ.get("DB_PORT", 5432),
                database = os.environ.get("DB_NAME", "transactions"),
                user = os.environ.get("DB_USER", "postgres"),
                password = os.environ.get("DB_PASSWORD", "postgres")
            )
            c = conn.cursor()
            c.execute("SELECT * FROM transactions")
            conn.close()
            return True
        except psycopg2.OperationalError:
            return False

    def __enter__(self):
        if not self.conn_available:
            # warn("Database not available, transactions will not be saved")
            Logger().info("Database not available, transactions will not be saved")
            return self
        
        self.c = self.conn.cursor()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if not self.conn_available:
            return
        
        self.conn.commit()
        self.conn.close()

    def write(self, transaccion: Transaccion) -> None:
        if not self.conn_available:
            return
        
        self.c.execute("SELECT id FROM transactions WHERE id = ?", (transaccion.id,))
        if self.c.fetchone() is None:
            self.c.execute("INSERT INTO transactions VALUES (?, ?, ?)", (transaccion.id, transaccion.date, transaccion.amount))