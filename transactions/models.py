from dataclasses import dataclass
from datetime import datetime
from decimal import Decimal

@dataclass
class Transaccion:
    id: int
    date: str
    amount: Decimal

    def __post_init__(self):
        if not self.date:
            raise ValueError("Date required")
        try:
            datetime.strptime(self.date, '%Y/%m/%d')
        except ValueError:
            raise ValueError("Incorrect data format, should be YYYY/MM/DD")
        if not self.amount:
            raise ValueError("Transaction amount required")

    