from transactions.services import process_transactions_file_content
from infrastructure.mailing import generate_email, send_email

def main():
    with open("trsx.csv", "r") as file:
        file_content = file.read()

    transactions_summary = process_transactions_file_content(file_content)
    email_body = generate_email(transactions_summary)

    send = send_email(email_body)

    # Save email to file if not sent
    if not send:
        with open("email.html", "w") as file:
            file.write(email_body)

if __name__ == "__main__":
    print("Starting application...")
    main()