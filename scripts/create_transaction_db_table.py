import os
import psycopg2

def create_db_postgresql():
    conn = psycopg2.connect(
                host = os.environ.get("DB_HOST", "localhost"),
                port = os.environ.get("DB_PORT", 5432),
                database = os.environ.get("DB_NAME", "transactions"),
                user = os.environ.get("DB_USER", "postgres"),
                password = os.environ.get("DB_PASSWORD", "postgres")
            )
    c = conn.cursor()
    c.execute("""
        CREATE TABLE transaction (
            id PRIMARY KEY,
            date DATE,
            amount DECIMAL
        )
    """)
    conn.commit()
    conn.close()

if __name__ == "__main__":
    create_db_postgresql()